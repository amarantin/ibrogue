//
//  Cell.swift
//  Brogue
//


import SpriteKit

public class MagCell: Cell {
    public convenience init(cell: Cell, magnify: CGFloat) {
        self.init(x: cell.position.x, y: cell.position.y, size: CGSize(width: cell.size.width * (magnify), height: cell.size.height * (magnify)))
        fgcolor = cell.fgcolor
        bgcolor = cell.bgcolor
        
        if cell.glyph != nil {
            glyph = cell.glyph
        }
    }
}

public class Cell {
    public let foreground: SKSpriteNode
    public let background: SKSpriteNode
    public var size: CGSize {
        get {
            return foreground.size
        }
        set(newSize) {
            foreground.size = newSize
            background.size = newSize
        }
    }
    
    public var position: CGPoint {
        get {
            return foreground.position
        }
        set(newPosition) {
            foreground.position = newPosition
            background.position = newPosition
        }
    }
    
    var glyph: SKTexture? {
        set(newGlyph) {
            foreground.texture = newGlyph
        }
        get {
            return foreground.texture
        }
    }
    
    public var fgcolor: SKColor {
        set(newColor) {
            foreground.color = newColor
        }
        get {
            return foreground.color
        }
    }
    
    public var bgcolor: SKColor {
        set(newColor) {
            background.color = newColor
        }
        get {
            return background.color
        }
    }
    
    public init(x: CGFloat, y: CGFloat, size: CGSize) {
        foreground = SKSpriteNode(color: .clear, size: size)
        background = SKSpriteNode(color: .white, size: size)
        
        // Allow colours to be changed by blending their white components
        foreground.colorBlendFactor = 1
        background.colorBlendFactor = 1
        
        // The positions should be static
        position = CGPoint(x: x, y: y)
        foreground.zPosition = 1 // Foreground
        
        background.anchorPoint = CGPoint(x: 0, y: 0)
        foreground.anchorPoint = CGPoint(x: 0, y: 0)
    }
}
