//
//  RogueUI.h
//  RogueUI
//
//  Created by Guitar on 2019-06-25.
//  Copyright © 2019 Amarantin Technology Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RogueUI.
FOUNDATION_EXPORT double RogueUIVersionNumber;

//! Project version string for RogueUI.
FOUNDATION_EXPORT const unsigned char RogueUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RogueUI/PublicHeader.h>

#define FOLIAGE_CHAR    0x2648 // Aries symbol
#define SCROLL_CHAR        0x266A//'?'        // 0x039E
#define CHARM_CHAR      0x03DF
#define RING_CHAR        0xffee
#define AMULET_CHAR        0x2640
#define WEAPON_CHAR        0x2191

